from PyQt5 import QtCore, QtGui, QtWidgets
import numpy as np
import math
import matplotlib.pyplot as plt
import time

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1100, 450)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.nextstep = QtWidgets.QPushButton(self.centralwidget)
        self.nextstep.setGeometry(QtCore.QRect(45, 190, 120, 50))
        self.nextstep.setObjectName("nextstep")
        self.nextstep.clicked.connect(self.button_next)
        self.porgreshnost1_user = QtWidgets.QLineEdit(self.centralwidget)
        self.porgreshnost1_user.setGeometry(QtCore.QRect(40, 80, 133, 20))
        self.porgreshnost1_user.setObjectName("porgreshnost_user")
        self.porgreshnost1_user.setValidator(QtGui.QRegExpValidator(QtCore.QRegExp("[-]{0,1}\d{0,}\.\d{0,}")))
        self.kolvo_tochek1 = QtWidgets.QLineEdit(self.centralwidget)
        self.kolvo_tochek1.setGeometry(QtCore.QRect(40, 130, 133, 20))
        self.kolvo_tochek1.setObjectName("kolvo_tochek")
        self.kolvo_tochek1.setValidator(QtGui.QRegExpValidator(QtCore.QRegExp("[-]{0,1}\d{0,}\.\d{0,}")))
        self.kolvo_tochek1_text = QtWidgets.QLabel(self.centralwidget)
        self.kolvo_tochek1_text.setGeometry(QtCore.QRect(87, 110, 300, 20))
        self.kolvo_tochek1_text.setObjectName("kolvo_tochek_text")
        self.porgreshnost1_text = QtWidgets.QLabel(self.centralwidget)
        self.porgreshnost1_text.setGeometry(QtCore.QRect(65, 60, 300, 15))
        self.porgreshnost1_text.setObjectName("kolvo_tochek_text_2")



        #---- table -----
        table = QtWidgets.QTableWidget(MainWindow)  # Создаём таблицу
        table.setColumnCount(4)     # Устанавливаем три колонки
        table.setRowCount(1)        # и одну строку в таблице
 
        # Устанавливаем заголовки таблицы
        table.setHorizontalHeaderLabels(["метод", "погрешность","шаг", "время"])
 
        # Устанавливаем всплывающие подсказки на заголовки
        table.horizontalHeaderItem(0).setToolTip("метод")
        table.horizontalHeaderItem(1).setToolTip("погрешность")
        table.horizontalHeaderItem(1).setToolTip("шаг")
        table.horizontalHeaderItem(2).setToolTip("время")
 
        # Устанавливаем выравнивание на заголовки
        table.horizontalHeaderItem(0).setTextAlignment(QtCore.Qt.AlignLeft)
        table.horizontalHeaderItem(1).setTextAlignment(QtCore.Qt.AlignHCenter)
        table.horizontalHeaderItem(2).setTextAlignment(QtCore.Qt.AlignRight)
        table.horizontalHeaderItem(3).setTextAlignment(QtCore.Qt.AlignRight)
 
        # заполняем первую строку
        #table.setItem(0, 0, QtWidgets.QTableWidgetItem("метод"))
        #table.setItem(0, 1, QtWidgets.QTableWidgetItem("погрешность"))
        #table.setItem(0, 2, QtWidgets.QTableWidgetItem("время"))
 
        # делаем ресайз колонок по содержимому
        table.resizeColumnsToContents()
 
        #grid_layout.addWidget(table, 0, 0)   # Добавляем таблицу в сетку
        self.scrollArea_2 = QtWidgets.QScrollArea(MainWindow)
        self.scrollArea_2.setGeometry(QtCore.QRect(340, 40,700,300))
        self.scrollArea_2.setWidgetResizable(True)
        self.scrollArea_2.setObjectName("scrollArea_2")
        self.scrollAreaWidgetContents_2 = QtWidgets.QWidget()
        self.scrollAreaWidgetContents_2.setGeometry(QtCore.QRect(0, 0, 249, 49))
        self.scrollAreaWidgetContents_2.setObjectName("scrollAreaWidgetContents_2")
        self.gridLayout_2 = QtWidgets.QGridLayout(self.scrollAreaWidgetContents_2)
        self.gridLayout_2.setObjectName("gridLayout_2")
        self.eiler_cifra = QtWidgets.QLabel(self.scrollAreaWidgetContents_2)
        self.eiler_cifra.setObjectName("pr_cifra")
        self.gridLayout_2.addWidget(table, 0, 0)
        self.scrollArea_2.setWidget(self.scrollAreaWidgetContents_2)

        self.result_table = table
        self.result_table.setObjectName("result_table")

        MainWindow.setCentralWidget(self.centralwidget)

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    
    def h(self):
        value_tochek1 = float(self.kolvo_tochek1.text())
        print(value_tochek1)
        return value_tochek1
    def pgr1(self):
        pgr_user1 = float(self.porgreshnost1_user.text())
        print(pgr_user1)
        return pgr_user1
    def button_next(self):
        def truncate_float(n, places):
            return int(n * (10 ** places)) / 10 ** places
        e = ui.pgr1()
        h = ui.h()
        a = 1
        b = 6
        def f(x, y):
            function = 2*(2-x)*y+(x+2)*math.exp(-x**2)*math.exp(3*x)
            return function
        def euler_method(a,b,h,e):
            heuler_method = h
            runge = 1
            start = time.perf_counter()
            while(runge > e):
                euler1 = np.empty(0)
                euler2 = np.empty(0)
                runge = np.empty(0)
                y = 10
                euler1 = np.append(euler1, 10)
                euler2 = np.append(euler2, 10)
                x = np.arange(a,(b+0.0000000000001),heuler_method)
                for i in x:
                    y = y + heuler_method * f(i, y)
                    euler1 = np.append(euler1, y)
                
                heuler_method /= 2
                y = 10
                x = np.arange(a,(b+0.0000000000001),heuler_method)
                for i in x:
                    y = y + heuler_method * f(i, y)
                    euler2 = np.append(euler2, y)
                euler1 = np.delete(euler1, euler1.size - 1)
                euler2 = np.delete(euler2, euler2.size - 1)
                for i in range(euler1.size-1):    
                    runge = np.append(runge,(abs((euler2[2*i + 2] - euler1[i + 1]))))
                runge = np.max(runge)
            d = dict(zip(x,euler2))
            with  open('euler.txt','w+') as file:
                file.write('x\t\ty[x]\n')
                for x2, y2 in d.items():
                    file.write('{}:{}\n'.format(x2,y2))
            
           
            plt.clf()
            plt.title("Метод Эйлера")
            plt.plot(x,euler2, color = 'cyan', linestyle='solid', linewidth=4)
            plt.savefig('euler.png')
            finish = time.perf_counter()
            function = round((finish - start),7)
            plt.show()
        
            return runge,heuler_method,function
        def hoyn_method(a,b,h,e):
            hhoyn_methoda = h
            runge = 1
            start = time.perf_counter()
            while(runge > e):
                runge = np.empty(0)
                hoyn_method1 = np.empty(0)
                hoyn_method2 = np.empty(0)
                y = 10
                hoyn_method1 = np.append(hoyn_method1, 10)
                hoyn_method2 = np.append(hoyn_method2, 10)
                x = np.arange(a,(b+0.0000000000001),hhoyn_methoda)
                for i in x:
                    y = y + (hhoyn_methoda/2) * (f(i, y) + f((i+hhoyn_methoda),(y + hhoyn_methoda * f(i, y))))
                    hoyn_method1 = np.append(hoyn_method1, y)
                hhoyn_methoda /= 2
                y = 10
                x = np.arange(a,(b+0.0000000000001),hhoyn_methoda)
                for i in x:
                    y = y + (hhoyn_methoda/2) * (f(i, y) + f((i+hhoyn_methoda),(y + hhoyn_methoda * f(i, y))))
                    hoyn_method2 = np.append(hoyn_method2, y)
                hoyn_method1 = np.delete(hoyn_method1, hoyn_method1.size - 1)
                hoyn_method2 = np.delete(hoyn_method2, hoyn_method2.size - 1)
                for i in range(hoyn_method1.size-1):    
                    runge = np.append(runge,(abs((hoyn_method2[2*i + 2] - hoyn_method1[i + 1])))/3)
                runge = np.max(runge)
                d = dict(zip(x,hoyn_method2))
                with  open('hoyn.txt','w+') as file:
                    file.write('x\t\ty[x]\n')
                    for x2, y2 in d.items():
                        file.write('{}:{}\n'.format(x2,y2))
                
            plt.clf()
            plt.title("Метод Хойна")
            plt.plot(x,hoyn_method2, color = 'blue', linestyle='dashdot', linewidth=4)
            plt.savefig('hoyn.png')
            finish = time.perf_counter()
            function = round((finish - start),7)
            plt.show()

            return runge,hhoyn_methoda,function
        def cauchy_method(a,b,h,e):
            hcauchy_method = h
            runge = 1
            start = time.perf_counter()
            while(runge > e):
                runge = np.empty(0)
                cauchy_method1 = np.empty(0)
                cauchy_method2 = np.empty(0)
                y = 10
                yk = [0]*5
                cauchy_method1 = np.append(cauchy_method1, 10)
                cauchy_method2 = np.append(cauchy_method2, 10)
                x = np.arange(a,(b+0.0000000000001),hcauchy_method)
                for i in x:
                    yk[0] = y + hcauchy_method * f(i, y)
                    for j in range(1,5,1):
                      yk[j] = y + hcauchy_method/2 * (f(i,y) + f((i + hcauchy_method), (yk[j-1])))
                    y = yk[4] 
                    cauchy_method1 = np.append(cauchy_method1, y)
                hcauchy_method /= 2
                y = 10
                x = np.arange(a,(b+0.0000000000001),hcauchy_method)
                for i in x:
                    yk[0] = y + hcauchy_method * f(i, y)
                    for j in range(1,5,1):
                      yk[j] = y + hcauchy_method/2 * (f(i,y) + f((i + hcauchy_method), (yk[j-1])))
                    y = yk[4] 
                    cauchy_method2 = np.append(cauchy_method2, y)
                cauchy_method1 = np.delete(cauchy_method1, cauchy_method1.size - 1)
                cauchy_method2 = np.delete(cauchy_method2, cauchy_method2.size - 1)
                for i in range(cauchy_method1.size-1):    
                    runge = np.append(runge,(abs((cauchy_method2[2*i + 2] - cauchy_method1[i + 1])))/3)
                runge = np.max(runge)
            d = dict(zip(x,cauchy_method2))
            with  open('cauchy.txt','w+') as file:
                file.write('x\t\ty[x]\n')
                for x2, y2 in d.items():
                    file.write('{}:{}\n'.format(x2,y2))
           
            plt.clf()
            plt.title("Метод Коши - Эйлера ")
            plt.plot(x,cauchy_method2, color = 'green', linewidth=4)
            plt.savefig('cauchy.png')
            finish = time.perf_counter()
            function = round((finish - start),7)
            plt.show()
            return runge,hcauchy_method,function

        def refined_euler_method(a,b,h,e):
            hrefined_euler_method = h
            runge = 1
            start = time.perf_counter()
            while(runge > e):
                runge = np.empty(0)
                refined_euler_method1 = np.empty(0)
                refined_euler_method2 = np.empty(0)
                yeuler_method = [0]*3
                yeuler_method[0] = 10
                refined_euler_method1 = np.append(refined_euler_method1, 10)
                refined_euler_method2 = np.append(refined_euler_method2, 10)
                x = np.arange(a,(b+0.0000000000001),hrefined_euler_method)
                yeuler_method[1] = yeuler_method[0] + hrefined_euler_method * f((x[0] + hrefined_euler_method/2), (yeuler_method[0] + hrefined_euler_method/2 * f(x[0],yeuler_method[0])))
                for i in x:
                    yeuler_method[2] = yeuler_method[0] + 2 * hrefined_euler_method * f((i+hrefined_euler_method),yeuler_method[1])
                    yeuler_method[0] = yeuler_method[1]
                    yeuler_method[1] = yeuler_method[2]
                    refined_euler_method1 = np.append(refined_euler_method1, yeuler_method[2])
                hrefined_euler_method /= 2
                yeuler_method[0] = 10
                x = np.arange(a,(b+0.0000000000001),hrefined_euler_method)
                yeuler_method[1] = yeuler_method[0] + hrefined_euler_method * f((x[0] + hrefined_euler_method/2), (yeuler_method[0] + hrefined_euler_method/2 * f(x[0],yeuler_method[0])))
                for i in x:
                    yeuler_method[2] = yeuler_method[0] + 2 * hrefined_euler_method * f((i+hrefined_euler_method),yeuler_method[1])
                    yeuler_method[0] = yeuler_method[1]
                    yeuler_method[1] = yeuler_method[2]
                    refined_euler_method2 = np.append(refined_euler_method2, yeuler_method[2])
                refined_euler_method1 = np.delete(refined_euler_method1, refined_euler_method1.size - 1)
                refined_euler_method2 = np.delete(refined_euler_method2, refined_euler_method2.size - 1)
                for i in range(refined_euler_method1.size-1):    
                    runge = np.append(runge,(abs((refined_euler_method2[2*i + 2] - refined_euler_method1[i + 1])))/3)
                runge = np.max(runge)
            d = dict(zip(x,refined_euler_method2))
            with  open('refined_euler.txt','w+') as file:
                file.write('x\t\ty[x]\n')
                for x2, y2 in d.items():
                    file.write('{}:{}\n'.format(x2,y2))
            
            finish = time.perf_counter()
            function = round((finish- start),7)
            plt.clf()
            plt.title("Уточнённый метод Эйлера")
            plt.plot(x,refined_euler_method2, color = 'yellow', linestyle=':', linewidth=4)
            plt.savefig('sophisticated_euler.png')
            plt.show()

            return runge,hrefined_euler_method, function
    
        def midpoint_method(a,b,h,e):
            hmidpoint_method = h
            runge = 1
            start = time.perf_counter()
            while(runge > e):
                runge = np.empty(0)
                midpoint_method1 = np.empty(0)
                midpoint_method2 = np.empty(0)
                y = 10
                midpoint_method1 = np.append(midpoint_method1, 10)
                midpoint_method2 = np.append(midpoint_method2, 10)
                x = np.arange(a,(b+0.0000000000001),hmidpoint_method)
                for i in x:
                    y = y + hmidpoint_method * f((i + hmidpoint_method/2),y + hmidpoint_method/2 * f(i,y))
                    midpoint_method1 = np.append(midpoint_method1, y)
                hmidpoint_method /= 2
                y = 10
                x = np.arange(a,(b+0.0000000000001),hmidpoint_method)
                for i in x:
                    y = y + hmidpoint_method * f((i + hmidpoint_method/2),y + hmidpoint_method/2 * f(i,y))
                    midpoint_method2 = np.append(midpoint_method2, y)
                midpoint_method1 = np.delete(midpoint_method1, midpoint_method1.size - 1)
                midpoint_method2 = np.delete(midpoint_method2, midpoint_method2.size - 1)
                for i in range(midpoint_method1.size-1):    
                    runge = np.append(runge,(abs((midpoint_method2[2*i + 2] - midpoint_method1[i + 1])))/3)
                runge = np.max(runge)
            d = dict(zip(x,midpoint_method2))
            with  open('midpoint.txt','w+') as file:
                file.write('x\t\ty[x]\n')
                for x2, y2 in d.items():
                    file.write('{}:{}\n'.format(x2,y2))
            
            plt.clf()
            plt.title("Метод средней точки")
            plt.plot(x,midpoint_method2, color = 'black', linestyle='-.', linewidth=4)
            plt.savefig('midpoint.png')
            finish = time.perf_counter()
            function = round((finish - start),7)
            plt.show()
            return runge,hmidpoint_method,function
        
        def runge_kutta_method(a,b,h,e):
            hrunge_kutta_method = h
            runge = 1
            start = time.perf_counter()
            while(runge > e):
                runge = np.empty(0)
                runge_kutta_method1 = np.empty(0)
                runge_kutta_method2 = np.empty(0)
                y = 10
                runge_kutta_method1 = np.append(runge_kutta_method1, 10)
                runge_kutta_method2 = np.append(runge_kutta_method2, 10)
                x = np.arange(a,(b+0.0000000000001),hrunge_kutta_method)
                for i in x:
                    n1 = f(i,y)
                    n2 = f((i + hrunge_kutta_method/2),( y + (hrunge_kutta_method/2) * n1))
                    n3 = f((i + hrunge_kutta_method/2),( y + (hrunge_kutta_method/2) * n2))
                    n4 = f((i + hrunge_kutta_method),( y + hrunge_kutta_method * n3))
                    y = y + (hrunge_kutta_method/6) * (n1 + 2*n2 + 2*n3 + n4)
                    runge_kutta_method1 = np.append(runge_kutta_method1, y)
                hrunge_kutta_method /= 2
                y = 10
                x = np.arange(a,(b+0.0000000000001),hrunge_kutta_method)
                for i in x:
                    n1 = f(i,y)
                    n2 = f((i + hrunge_kutta_method/2),( y + (hrunge_kutta_method/2) * n1))
                    n3 = f((i + hrunge_kutta_method/2),( y + (hrunge_kutta_method/2) * n2))
                    n4 = f((i + hrunge_kutta_method),( y + hrunge_kutta_method * n3))
                    y = y + (hrunge_kutta_method/6) * (n1 + 2*n2 + 2*n3 + n4)
                    runge_kutta_method2 = np.append(runge_kutta_method2, y)
                runge_kutta_method1 = np.delete(runge_kutta_method1, runge_kutta_method1.size - 1)
                runge_kutta_method2 = np.delete(runge_kutta_method2, runge_kutta_method2.size - 1)
                for i in range(runge_kutta_method1.size-1):    
                    runge = np.append(runge,(abs((runge_kutta_method2[2*i + 2] - runge_kutta_method1[i + 1])))/15)
                runge = np.max(runge)
            d = dict(zip(x,runge_kutta_method2))
            with  open('runge_kutta.txt','w+') as file:
                file.write('x\t\ty[x]\n')
                for x2, y2 in d.items():
                    file.write('{}:{}\n'.format(x2,y2))
           
            plt.clf()
            plt.title("Метод Рунге-Кутты 4-го порядка")
            plt.plot(x,runge_kutta_method2, color = 'red', linestyle='dashed', linewidth=4)
            plt.savefig('runge_kutta.png')
            finish = time.perf_counter()
            function = round((finish - start),7)
            plt.show()
            return runge,hrunge_kutta_method, function    
       
        _translate = QtCore.QCoreApplication.translate
        #Dialog.setWindowTitle(_translate("Dialog", "Dialog"))
        rowPosition = self.result_table.rowCount()
        self.result_table.insertRow(rowPosition)
        result = euler_method(a,b,h,e)
        self.result_table.setItem(rowPosition-1, 0, QtWidgets.QTableWidgetItem("Метод Эйлера"))
        self.result_table.setItem(rowPosition-1, 1, QtWidgets.QTableWidgetItem(str(truncate_float(result[0],7))))
        self.result_table.setItem(rowPosition-1, 2, QtWidgets.QTableWidgetItem(str(truncate_float(result[1],7))))
        self.result_table.setItem(rowPosition-1, 3, QtWidgets.QTableWidgetItem(str(truncate_float(result[2],7))))

        rowPosition = self.result_table.rowCount()
        self.result_table.insertRow(rowPosition)
        result = hoyn_method(a,b,h,e)
        self.result_table.setItem(rowPosition-1, 0, QtWidgets.QTableWidgetItem("Метод Хойна"))
        self.result_table.setItem(rowPosition-1, 1, QtWidgets.QTableWidgetItem(str(truncate_float(result[0],7))))
        self.result_table.setItem(rowPosition-1, 2, QtWidgets.QTableWidgetItem(str(truncate_float(result[1],7))))
        self.result_table.setItem(rowPosition-1, 3, QtWidgets.QTableWidgetItem(str(truncate_float(result[2],7))))

        rowPosition = self.result_table.rowCount()
        self.result_table.insertRow(rowPosition)
        result = cauchy_method(a,b,h,e)
        self.result_table.setItem(rowPosition-1, 0, QtWidgets.QTableWidgetItem("Метод Коши"))
        self.result_table.setItem(rowPosition-1, 1, QtWidgets.QTableWidgetItem(str(truncate_float(result[0],7))))
        self.result_table.setItem(rowPosition-1, 2, QtWidgets.QTableWidgetItem(str(truncate_float(result[1],7))))
        self.result_table.setItem(rowPosition-1, 3, QtWidgets.QTableWidgetItem(str(truncate_float(result[2],7))))

        rowPosition = self.result_table.rowCount()
        self.result_table.insertRow(rowPosition)
        result = refined_euler_method(a,b,h,e)
        self.result_table.setItem(rowPosition-1, 0, QtWidgets.QTableWidgetItem("Уточнённый метод Эйлера"))
        self.result_table.setItem(rowPosition-1, 1, QtWidgets.QTableWidgetItem(str(truncate_float(result[0],7))))
        self.result_table.setItem(rowPosition-1, 2, QtWidgets.QTableWidgetItem(str(truncate_float(result[1],7))))
        self.result_table.setItem(rowPosition-1, 3, QtWidgets.QTableWidgetItem(str(truncate_float(result[2],7))))

        rowPosition = self.result_table.rowCount()
        self.result_table.insertRow(rowPosition)
        result = midpoint_method(a,b,h,e)
        self.result_table.setItem(rowPosition-1, 0, QtWidgets.QTableWidgetItem("Метод средней точки"))
        self.result_table.setItem(rowPosition-1, 1, QtWidgets.QTableWidgetItem(str(truncate_float(result[0],7))))
        self.result_table.setItem(rowPosition-1, 2, QtWidgets.QTableWidgetItem(str(truncate_float(result[1],7))))
        self.result_table.setItem(rowPosition-1, 3, QtWidgets.QTableWidgetItem(str(truncate_float(result[2],7))))

        rowPosition = self.result_table.rowCount()
        self.result_table.insertRow(rowPosition)
        result = runge_kutta_method(a,b,h,e)
        self.result_table.setItem(rowPosition-1, 0, QtWidgets.QTableWidgetItem("Метод Рунге-Кутта"))
        self.result_table.setItem(rowPosition-1, 1, QtWidgets.QTableWidgetItem(str(truncate_float(result[0],7))))
        self.result_table.setItem(rowPosition-1, 2, QtWidgets.QTableWidgetItem(str(truncate_float(result[1],7))))
        self.result_table.setItem(rowPosition-1, 3, QtWidgets.QTableWidgetItem(str(truncate_float(result[2],7))))

       
    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.nextstep.setText(_translate("MainWindow", "Посчитать"))
        self.kolvo_tochek1_text.setText(_translate("MainWindow", "Шаг"))
        self.porgreshnost1_text.setText(_translate("MainWindow", "Погрешность"))
      
       
if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    MainWindow = QtWidgets.QMainWindow()
    ui = Ui_MainWindow()
    ui.setupUi(MainWindow)
    MainWindow.show()
    app.exec()
    sys.exit(app.exec())
